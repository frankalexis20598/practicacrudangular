import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/servicios/auth.service';

@Component({
  selector: 'app-navegacion',
  templateUrl: './navegacion.component.html',
  styleUrls: ['./navegacion.component.css']
})
export class NavegacionComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onClickCerrarSesion() {
    this.authService.cerrarSesion()
      .subscribe(res => {
        if (res) {
          console.log('Cerrar Sesión');
          this.router.navigate(['/login']);
        }
      });
  }

}
