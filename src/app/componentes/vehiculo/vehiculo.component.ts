import { Component, OnInit, Input } from '@angular/core';
import { IVehiculo, ITipoVehiculo } from 'src/app/modelos/vehiculo.interface';
import { TipoVehiculoService } from 'src/app/servicios/tipo-vehiculo.service';
import { NgForm } from '@angular/forms';
import { VehiculoService } from 'src/app/servicios/vehiculo.service';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css']
})
export class VehiculoComponent implements OnInit {
  @Input() visualizacion: boolean;

  listaVehiculos: IVehiculo[] = [];
  temporalVehiculo: IVehiculo = { tipo: { nombre: 'MOTO', id: 1 }, propietario: {} };
  listaTipoVehiculos: ITipoVehiculo[] = [];
  temporalTipoVehiculo: ITipoVehiculo = {};
  selectedVehiculo: IVehiculo = { tipo: { nombre: 'MOTO', id: 1 }, propietario: {} };
  activarForm = false;
  editarVehiculo = false;
  indiceVehiculo: number;
  detalleModal = false;

  constructor(private tipoVehiculoService: TipoVehiculoService,
    private vehiculoService: VehiculoService) { }

  ngOnInit() {
    this.obtenerTipoVehiculos();
    this.obtenerVehiculos();
  }

  obtenerTipoVehiculos() {
    this.tipoVehiculoService.listarTipoVehiculos()
      .subscribe(tipoVehiculo => this.listaTipoVehiculos = tipoVehiculo);
  }

  obtenerVehiculos() {
    this.vehiculoService.listarVehiculos()
      .subscribe(vehiculos => this.listaVehiculos = vehiculos);
  }

  onClickVerVehiculo(vehiculo: IVehiculo) {
    this.selectedVehiculo = this.clonar(vehiculo);
    this.detalleModal = true;
  }

  onClickEditarVehiculo(vehiculo: IVehiculo, indice: number) {
    this.editarVehiculo = true;
    this.indiceVehiculo = indice;
    this.temporalVehiculo = this.clonar(vehiculo);
    this.listaTipoVehiculos.forEach(element => {
      if (element.id === this.temporalVehiculo.tipo.id) {
        this.temporalVehiculo.tipo = element;
      }
    });
    this.mostrarForm();
  }

  onClickEliminarVehiculo(indice: number) {
    if (confirm('¿Está seguro que desea eliminar el registro?')) {
      this.vehiculoService.eliminarVehiculo(indice)
        .subscribe(msg => {
          if (msg.estado) {
            alert(msg.mensaje);
          } else {
            alert('Error al eliminar el vehículo!');
          }
          this.resetearTemporalVehiculo();
        });
    }
  }

  submitGuardarVehiculo(form: NgForm) {
    if (form.valid) {
      if (this.editarVehiculo) {
        this.vehiculoService.actualizarVehiculo(this.temporalVehiculo, this.indiceVehiculo)
          .subscribe(msg => {
            if (msg.estado) {
              console.log(msg.mensaje);
            } else {
              alert('Error al actualizar el vehículo!');
            }
            this.resetearFormulario();
          });
      } else {
        this.vehiculoService.guardarVehiculo(this.temporalVehiculo)
          .subscribe(msg => {
            if (msg.estado) {
              console.log(msg.mensaje);
            } else {
              alert('Error al guardar el vehículo!');
            }
            this.resetearFormulario();
          });
      }
    }
  }

  private resetearFormulario() {
    this.resetearTemporalVehiculo();
    this.editarVehiculo = false;
    this.ocultarForm();
  }

  mostrarForm() {
    this.activarForm = true;
  }

  ocultarForm() {
    this.activarForm = false;
    this.resetearTemporalVehiculo();
  }

  private resetearTemporalVehiculo() {
    this.temporalVehiculo = { tipo: { nombre: 'MOTO', id: 1 }, propietario: {} };
  }

  public clonar(objeto: any): any {
    const r: any = {};
    if (objeto == null) {
      return null;
    }
    for (const prop in objeto) {
      if (objeto[prop] != null && objeto[prop] instanceof Array) {
        const lista: any = objeto[prop];
        if (lista.length === 0) {
          r[prop] = [];
        }
        if (lista.length > 0) {
          if (typeof lista[0] === 'string' || typeof lista[0] === 'number') {
            r[prop] = lista;
          } else {
            r[prop] = this.clonar_lista(objeto[prop]);
          }
        }
      } else if (objeto[prop] instanceof Object) {
        r[prop] = this.clonar(objeto[prop]);
      } else {
        r[prop] = objeto[prop];
      }
      // r[prop] = this.clonar(objeto[prop]);
    }
    return r;
  }

  public clonar_lista(lista: any): any {
    const r: any = [];
    lista.forEach((element) => {
      const re = this.clonar(element);
      r.push(re);
    });
    return r;
  }

}
