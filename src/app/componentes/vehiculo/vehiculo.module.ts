import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehiculoComponent } from './vehiculo.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'primeng/datatable';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  declarations: [VehiculoComponent],
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ButtonModule,
    CardModule,
    InputTextModule,
    DropdownModule,
    DialogModule
  ],
  exports: [VehiculoComponent]
})
export class VehiculoModule { }
