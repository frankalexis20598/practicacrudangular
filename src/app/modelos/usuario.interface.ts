export interface IUsuario {
    id?: number;
    nombre?: string;
    rol?: string;
    correo?: string;
    password?: string;
}

export interface IAuth {
    mensaje?: string;
    estado?: boolean;
}
