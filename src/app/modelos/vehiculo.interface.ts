export interface IVehiculo {
    tipo?: ITipoVehiculo;
    marca?: string;
    color?: string;
    placa?: string;
    km?: number;
    tiempoUso?: number;
    motor?: string;
    propietario?: IPropietario;
}

export interface IPropietario {
    nombre?: string;
    apellido?: string;
    dni?: string;
    numLicencia?: string;
}

export interface ITipoVehiculo {
    nombre?: string;
    id?: number;
}