import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministracionComponent } from './administracion.component';
import { Routes, RouterModule } from '@angular/router';
import { NavegacionModule } from 'src/app/componentes/navegacion/navegacion.module';
import { PanelModule } from 'primeng/panel';
import { VehiculoModule } from 'src/app/componentes/vehiculo/vehiculo.module';
import { AuthGuard } from 'src/app/guardias/auth.guard';
import { AdminGuard } from 'src/app/guardias/admin.guard';

export const rutas: Routes = [
  { path: '', component: AdministracionComponent, canActivate: [AuthGuard, AdminGuard] }
];

@NgModule({
  declarations: [AdministracionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(rutas),
    NavegacionModule,
    PanelModule,
    VehiculoModule
  ]
})
export class AdministracionModule { }
