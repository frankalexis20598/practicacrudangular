import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisualizacionComponent } from './visualizacion.component';
import { Routes, RouterModule } from '@angular/router';
import { NavegacionModule } from 'src/app/componentes/navegacion/navegacion.module';
import { PanelModule } from 'primeng/panel';
import { VehiculoModule } from 'src/app/componentes/vehiculo/vehiculo.module';
import { AuthGuard } from 'src/app/guardias/auth.guard';
import { UserGuard } from 'src/app/guardias/user.guard';

export const rutas: Routes = [
  { path: '', component: VisualizacionComponent, canActivate: [AuthGuard, UserGuard] }
];

@NgModule({
  declarations: [VisualizacionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(rutas),
    NavegacionModule,
    PanelModule,
    VehiculoModule
  ]
})
export class VisualizacionModule { }
