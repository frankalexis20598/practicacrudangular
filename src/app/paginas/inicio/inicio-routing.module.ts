import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  { path: 'administracion', loadChildren: './administracion/administracion.module#AdministracionModule' },
  { path: 'visualizacion', loadChildren: './visualizacion/visualizacion.module#VisualizacionModule' },
  { path: '**', pathMatch: 'full', redirectTo: '/login' },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class InicioRoutingModule { }
