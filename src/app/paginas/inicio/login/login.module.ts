import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule, Routes } from '@angular/router';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { LoginGuard } from 'src/app/guardias/login.guard';

export const rutas: Routes = [
  { path: '', component: LoginComponent, canActivate: [LoginGuard] }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(rutas),
    PanelModule,
    ButtonModule,
    InputTextModule,
    FormsModule
  ]
})
export class LoginModule { }
