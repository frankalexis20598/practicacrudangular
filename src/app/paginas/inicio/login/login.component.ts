import { Component, OnInit } from '@angular/core';
import { IUsuario } from 'src/app/modelos/usuario.interface';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/servicios/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario: IUsuario = {};

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  submitIniciarSesion(form: NgForm) {
    if (form.valid) {
      this.authService.iniciarSesion(this.usuario.correo, this.usuario.password)
        .subscribe(res => {
          if (res.estado) {
            this.authService.obtenerUsuarioActual().subscribe(user => this.usuario = user);
            if (this.usuario.rol === 'ADMINISTRADOR') {
              this.router.navigate(['/administracion']);
            } else {
              this.router.navigate(['/visualizacion']);
            }
          } else {
            alert(res.mensaje);
          }
        });
    } else {
      console.log('error de validacion');
    }
  }

}
