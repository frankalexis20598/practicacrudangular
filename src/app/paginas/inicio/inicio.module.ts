import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio.component';
import { InicioRoutingModule } from './inicio-routing.module';
import { AuthGuard } from 'src/app/guardias/auth.guard';
import { LoginGuard } from 'src/app/guardias/login.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminGuard } from 'src/app/guardias/admin.guard';
import { UserGuard } from 'src/app/guardias/user.guard';

@NgModule({
  declarations: [InicioComponent],
  imports: [
    CommonModule,
    InicioRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [AuthGuard, LoginGuard, AdminGuard, UserGuard]
})
export class InicioModule { }
