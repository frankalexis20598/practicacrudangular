import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../servicios/auth.service';
import { IUsuario } from '../modelos/usuario.interface';

@Injectable()
export class AuthGuard implements CanActivate {
  usuarioActual: IUsuario;
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.comprobarInicioSesion()) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}
