import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../servicios/auth.service';
import { IUsuario } from '../modelos/usuario.interface';

@Injectable()
export class LoginGuard implements CanActivate {
  usuarioActual: IUsuario;

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authService.comprobarInicioSesion()) {
      return true;
    }

    this.authService.obtenerUsuarioActual()
      .subscribe(usuario => {
        this.usuarioActual = usuario;
      });
    if (this.usuarioActual.rol === 'ADMINISTRADOR') {
      this.router.navigate(['/administracion']);
    } else {
      this.router.navigate(['/visualizacion']);
    }
    return false;
  }
}
