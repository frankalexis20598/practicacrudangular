import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../servicios/auth.service';
import { IUsuario } from '../modelos/usuario.interface';
import { Location } from '@angular/common';

@Injectable()
export class UserGuard implements CanActivate {
  usuarioActual: IUsuario;
  constructor(private authService: AuthService, private location: Location) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.authService.obtenerUsuarioActual()
      .subscribe(usuario => this.usuarioActual = usuario);

    if (this.usuarioActual.rol === 'USUARIO') {
      return true;
    }

    this.location.back();
    return false;
  }
}
