import { Injectable } from '@angular/core';
import { IUsuario, IAuth } from '../modelos/usuario.interface';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly USUARIOS: IUsuario[] = [
    {
      id: 1,
      nombre: 'Frank Alexis',
      rol: 'ADMINISTRADOR',
      correo: 'admin@solmit.net',
      password: '12345678910'
    },
    {
      id: 2,
      nombre: 'Steven Andrés',
      rol: 'USUARIO',
      correo: 'user@solmit.net',
      password: '10987654321'
    }
  ];

  constructor() { }

  comprobarInicioSesion(): boolean {
    if (localStorage.getItem('usuarioActual') !== null) {
      return true;
    }
    return false;
  }

  iniciarSesion(correo: string, password: string): Observable<IAuth> {
    const resultado = this.USUARIOS.find(usuario => {
      return (usuario.correo === correo && usuario.password === password);
    });
    if (resultado !== undefined) {
      localStorage.setItem('usuarioActual', JSON.stringify(resultado));
      return of(this.crearRespuesta('Inicio de Sesión exitoso.', true));
    } else {
      return of(this.crearRespuesta('Credenciales incorrectas!', false));
    }
  }

  private crearRespuesta(mensaje: string, estado: boolean): IAuth {
    const auth: IAuth = {};
    auth.mensaje = mensaje;
    auth.estado = estado;
    return auth;
  }

  cerrarSesion(): Observable<boolean> {
    localStorage.removeItem('usuarioActual');
    return of(true);
  }

  obtenerUsuarioActual(): Observable<IUsuario> {
    const usuario = JSON.parse(localStorage.getItem('usuarioActual'));
    return of(usuario);
  }

}
