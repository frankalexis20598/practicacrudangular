import { Injectable } from '@angular/core';
import { ITipoVehiculo } from '../modelos/vehiculo.interface';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TipoVehiculoService {
  private readonly LISTA_TIPO_VEHICULO: ITipoVehiculo[] = [
    { nombre: 'MOTO', id: 1 },
    { nombre: 'CAMION', id: 2 },
    { nombre: 'CARRO', id: 3 }
  ];

  constructor() { }

  listarTipoVehiculos(): Observable<ITipoVehiculo[]> {
      return of(this.LISTA_TIPO_VEHICULO);
  }
}
