import { Injectable } from '@angular/core';
import { IVehiculo } from '../modelos/vehiculo.interface';
import { Observable, of } from 'rxjs';
import { IMessage } from '../modelos/message.interface';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {
  private readonly LISTA_VEHICULOS: IVehiculo[] = [
    {
      tipo: { nombre: 'CARRO', id: 3 },
      marca: 'Toyota',
      color: 'Azul',
      placa: '458ESD',
      propietario: {
        nombre: 'Frank',
        apellido: 'Tomaylla',
        dni: '985135468',
        numLicencia: '123456'
      }
    },
    {
      tipo: { nombre: 'CAMION', id: 2 },
      marca: 'Toyota',
      color: 'Azul',
      placa: '458ESD',
      propietario: {
        nombre: 'Frank',
        apellido: 'Tomaylla',
        dni: '985135468',
        numLicencia: '123456'
      }
    },
    {
      tipo: { nombre: 'MOTO', id: 1 },
      marca: 'Toyota',
      color: 'Azul',
      placa: '458ESD',
      km: 100,
      tiempoUso: 1,
      motor: 'Motor monocilíndrico',
      propietario: {
        nombre: 'Frank',
        apellido: 'Tomaylla',
        dni: '985135468',
        numLicencia: '123456'
      }
    },
    {
      tipo: { nombre: 'CARRO', id: 3 },
      marca: 'Toyota',
      color: 'Azul',
      placa: '458ESD',
      propietario: {
        nombre: 'Frank',
        apellido: 'Tomaylla',
        dni: '985135468',
        numLicencia: '123456'
      }
    }
  ];
  msg: IMessage = { estado: false };

  constructor() { }

  listarVehiculos(): Observable<IVehiculo[]> {
    return of(this.LISTA_VEHICULOS);
  }

  guardarVehiculo(vehiculo: IVehiculo): Observable<IMessage> {
    this.LISTA_VEHICULOS.push(vehiculo);
    this.msg.mensaje = 'El vehículo se guardo correctamente.';
    this.msg.estado = true;
    return of(this.msg);
  }

  actualizarVehiculo(vehiculo: IVehiculo, indice: number): Observable<IMessage> {
    this.LISTA_VEHICULOS[indice] = vehiculo;
    this.msg.mensaje = 'El vehículo se actualizó correctamente.';
    this.msg.estado = true;
    return of(this.msg);
  }

  eliminarVehiculo(indice: number): Observable<IMessage> {
    this.LISTA_VEHICULOS.splice(indice, 1);
    this.msg.mensaje = 'El vehículo se elimino correctamente.';
    this.msg.estado = true;
    return of(this.msg);
  }
}
